pragma solidity >=0.4.21 <0.6.0;

import "./nf-token-enumerable.sol";
import "./nf-token-metadata.sol";
import "./nf-token.sol";
import "../ownership/ownable.sol";

/**
 * @dev This is an example contract implementation of NFToken with metadata extension.
 */
contract ARGJuniors is
  NFTokenEnumerable, NFTokenMetadata, Ownable
{

  /**
   * @dev Contract constructor. Sets metadata extension `name` and `symbol`.
   */
constructor () public
        
{
    nftName = "Argentinos Juniors Coin";
    nftSymbol = "ARGJ";
}
  // Mapping from owner to list of owned token IDs
    mapping(address => uint256[]) private _ownedTokens;
  /**
   * @dev Mints a new NFT.
   * @param _to The address that will own the minted NFT.
   * @param _tokenId of the NFT to be minted by the msg.sender.
   * @param _uri String representing RFC 3986 URI.
   */
  function mint(
    address _to,
    uint256 _tokenId,
    string calldata _uri
  )
    external
    
  {
    super._mint(_to, _tokenId);
    _setURI(_tokenId, _uri);
  }
  function setURI(
    uint256 _tokenId,
    string calldata _uri
  )
    external
    
  {
    super.setTokenUri(_tokenId, _uri);
  }
  function _setURI(
    uint256 _tokenId,
    string memory _uri
  )
    internal
    
  {
    super.setTokenUri(_tokenId, _uri);
  }
  function burnToken (uint256 _tokenId) external
    
  {
    super._burnToken(_tokenId);

  }
   function tokensOfOwners(address _owner) public view returns(uint256[] 
     memory ownerTokens)
   {
        
        uint256[] memory tokensOfOwner = super.tokensOfOwner(_owner);
	      return tokensOfOwner;        
   }
  function totalSupply() public view returns(uint256)
   {
        
       uint256 supply = super._totalSupply();
       return supply;

   }
  
}


